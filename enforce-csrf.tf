resource "aws_wafregional_size_constraint_set" "match_csrf_token" {
  name = "${var.waf_prefix}-size-restrictions"

  size_constraints {
    text_transformation = "NONE"
    comparison_operator = "EQ"
    size                = "36"

    field_to_match {
      type = "HEADER"
      data = "x-csrf-token"
    }
  }
}

resource "aws_wafregional_byte_match_set" "byte_match_csrf_method" {
  name = "${var.waf_prefix}-match-csrf-method"

  byte_match_tuples {
    text_transformation   = "LOWERCASE"
    target_string         = "post"
    positional_constraint = "EXACTLY"

    field_to_match {
      type = "METHOD"
    }
  }
}

resource "aws_wafregional_rule" "enforce_csrf" {
  name        = "${var.waf_prefix}-enforce-csrf"
  metric_name = "${var.waf_prefix}EnforceCsrf"

  predicate {
    type    = "SizeConstraint"
    data_id = "${aws_wafregional_size_constraint_set.match_csrf_token.id}"
    negated = true
  }

  predicate {
    type    = "ByteMatch"
    data_id = "${aws_wafregional_byte_match_set.byte_match_csrf_method.id}"
    negated = false
  }
}
