resource "aws_wafregional_ipset" "match_blacklisted_ips" {
  name = "${var.waf_prefix}-match-blacklisted-ips"

  ip_set_descriptor {
    type  = "IPV4"
    value = "192.168.0.0/16"
  }

  ip_set_descriptor {
    type  = "IPV4"
    value = "169.254.0.0/16"
  }

  ip_set_descriptor {
    type  = "IPV4"
    value = "172.16.0.0/16"
  }

  ip_set_descriptor {
    type  = "IPV4"
    value = "10.0.0.0/8"
  }

  ip_set_descriptor {
    type  = "IPV4"
    value = "127.0.0.1/32"
  }
}

resource "aws_wafregional_rule" "detect_blacklisted_ips" {
  name        = "${var.waf_prefix}-detect-blacklisted-ips"
  metric_name = "${var.waf_prefix}DetectBlacklistedIps"

  predicate {
    type    = "IPMatch"
    data_id = "${aws_wafregional_ipset.match_blacklisted_ips.id}"
    negated = false
  }
}
