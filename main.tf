provider "aws" {
  region = "${var.region}"
}

locals {
  tags = {
    Project = "${var.project}"
  }
}
