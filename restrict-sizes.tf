resource "aws_wafregional_size_constraint_set" "size_constraint_set" {
  name = "${var.waf_prefix}-size-restrictions"

  size_constraints {
    text_transformation = "NONE"
    comparison_operator = "GT"
    size                = "4093"

    field_to_match {
      type = "HEADER"
      data = "Cookie"
    }
  }

  size_constraints {
    text_transformation = "NONE"
    comparison_operator = "GT"
    size                = "4096"

    field_to_match {
      type = "BODY"
    }
  }

  size_constraints {
    text_transformation = "NONE"
    comparison_operator = "GT"
    size                = "1024"

    field_to_match {
      type = "QUERY_STRING"
    }
  }

  size_constraints {
    text_transformation = "NONE"
    comparison_operator = "GT"
    size                = "512"

    field_to_match {
      type = "URI"
    }
  }
}

resource "aws_wafregional_rule" "restrict_sizes" {
  name        = "${var.waf_prefix}-restrict-sizes"
  metric_name = "${var.waf_prefix}RestrictSizes"

  predicate {
    type    = "SizeConstraint"
    data_id = "${aws_wafregional_size_constraint_set.size_constraint_set.id}"
    negated = false
  }
}
