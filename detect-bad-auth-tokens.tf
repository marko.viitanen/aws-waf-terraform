resource "aws_wafregional_byte_match_set" "byte_match_auth_tokens" {
  name = "${var.waf_prefix}-match-auth-tokens"

  byte_match_tuples {
    text_transformation   = "URL_DECODE"
    target_string         = ".TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ"
    positional_constraint = "ENDS_WITH"

    field_to_match {
      type = "HEADER"
      data = "authorization"
    }
  }

  byte_match_tuples {
    text_transformation   = "URL_DECODE"
    target_string         = "example-session-id"
    positional_constraint = "CONTAINS"

    field_to_match {
      type = "HEADER"
      data = "cookie"
    }
  }
}

resource "aws_wafregional_rule" "detect_bad_auth_tokens" {
  name        = "${var.waf_prefix}-detect-bad-auth-tokens"
  metric_name = "${var.waf_prefix}DetectBadAuthTokens"

  predicate {
    type    = "ByteMatch"
    data_id = "${aws_wafregional_byte_match_set.byte_match_auth_tokens.id}"
    negated = false
  }
}
