variable "region" {
  type    = "string"
  default = "eu-west-1"
}

variable "availability_zone_a" {
  type    = "string"
  default = "eu-west-1a"
}

variable "availability_zone_b" {
  type    = "string"
  default = "eu-west-1b"
}

variable "availability_zone_c" {
  type    = "string"
  default = "eu-west-1c"
}

variable "project" {
  type    = "string"
  default = "marko-waf"
}

variable "waf_prefix" {
  type    = "string"
  default = "MarkoKoulu"
}

variable "aws_access_key" {
  default = ""
}

variable "aws_secret_key" {
  default = ""
}
