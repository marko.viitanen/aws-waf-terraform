resource "aws_wafregional_ipset" "admin_remote_ip" {
  name = "match-admin-remote-ip"

  ip_set_descriptor {
    type  = "IPV4"
    value = "127.0.0.1/32"
  }
}

resource "aws_wafregional_byte_match_set" "byte_match_admin" {
  name = "${var.waf_prefix}-match-admin-url"

  byte_match_tuples {
    text_transformation   = "NONE"
    target_string         = "/admin"
    positional_constraint = "STARTS_WITH"

    field_to_match {
      type = "URI"
    }
  }
}

resource "aws_wafregional_rule" "detect_admin_access" {
  name        = "${var.waf_prefix}-detect-admin-access"
  metric_name = "${var.waf_prefix}DetectAdminAccess"

  predicate {
    type    = "IPMatch"
    data_id = "${aws_wafregional_ipset.admin_remote_ip.id}"
    negated = true
  }

  predicate {
    type    = "ByteMatch"
    data_id = "${aws_wafregional_byte_match_set.byte_match_admin.id}"
    negated = false
  }
}
